# FlowSure
FlowSure is a period tracking and task management app.

**Target Audience:**  Women.

# Features

* Used by women with irregular or regular periods.
* Estimated period dates upto 12 months.
* Track your period dates and plan your events accordingly.
* Exercises to ease cramps.
* Interact with community of women and share your experiences.
* Get healthy food recipes and also share your favourite recipes to the world.

# Snapshots
<img src="/snapshots/login.jpeg"  width="30%" height="40%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="/snapshots/workout.jpeg"  width="30%" height="40%">
<br>
<br>
<img src="/snapshots/calender.jpeg"  width="30%" height="40%">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<img src="/snapshots/community.jpeg"  width="30%" height="40%">

# Demonstration
https://www.instagram.com/tv/CGUeP2fpa7Z/?utm_source=ig_web_copy_link
