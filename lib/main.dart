import 'package:flowSure/ui/calendar/calendar_home.dart';
import 'package:flowSure/ui/posts/single_post_screen.dart';
import 'package:flowSure/ui/user/demo.dart';
import 'package:flowSure/ui/user/my_posts.dart';
import 'package:flowSure/ui/user/user.dart';
import 'package:flowSure/ui/wrapper.dart';
import 'package:provider/provider.dart';
import 'ui/user/auth.dart';
import 'package:flowSure/ui/posts/new_post.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/user/user_home.dart';
import 'package:flowSure/ui/posts/posts_home.dart';
import 'package:flowSure/ui/health/health_home.dart';
import 'package:flowSure/ui/exercise_section/exercise_home.dart';
import 'package:flowSure/ui/calendar/calendar_page.dart';

import "package:flowSure/ui/exercise_section/screens/third_screen.dart";
import 'package:flowSure/main.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/models/activity.dart';
import 'package:flowSure/models/moodcard.dart';
import 'package:flowSure/screens/chart.dart';
import 'package:flowSure/screens/homepage.dart';
import 'package:flowSure/screens/start.dart';
import 'package:provider/provider.dart';

void main() => runApp(FlowSure());

class FlowSure extends StatefulWidget {
  @override
  _FlowSureState createState() => _FlowSureState();
}

class _FlowSureState extends State<FlowSure> {
  @override
  Widget build(BuildContext context) {
    return StreamProvider<User>.value(
      value: AuthService().user,
      child: MaterialApp(
        title: "FlowSure",
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => Wrapper(),
          '/calender': (context) => Calendar(),
          '/user': (context) => UserMenu(),
          '/health': (context) => Health(),
          '/posts': (context) => PostsHome(),
          '/exercise_section': (context) => Exercise(),
          ExerciseDetailsScreen.routeName: (ctx) => ExerciseDetailsScreen(),
          '/newpost': (context) => NewPost(),
          '/calendar_month': (context) => CalendarMonth(),
          '/myposts': (context) => MyPosts(),
          '/home': (context) => Home(),
          '/home_screen': (context) => MoodHomeScreen(),
          '/chart': (context) => MoodChart(),
        },
      ),
    );
  }
}
