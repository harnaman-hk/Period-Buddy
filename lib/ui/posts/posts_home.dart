import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowSure/ui/layouts/header.dart';
import 'package:flowSure/ui/posts/post_model.dart';
import 'package:flowSure/ui/posts/posts_database.dart';
import 'package:flowSure/ui/posts/posts_list.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/layouts/navbar.dart';
import 'package:provider/provider.dart';

class PostsHome extends StatefulWidget {
  @override
  _PostsHomeState createState() => _PostsHomeState();
}

class _PostsHomeState extends State<PostsHome> {
  TextEditingController searchTextEditingController = TextEditingController();
  Future<QuerySnapshot> searchResults;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _currentUserUid;

  @override
  void initState() {
    super.initState();
    initUser();
  }

  initUser() async {
    _currentUserUid = (await _auth.currentUser()).uid;
    print("\n\ncurrent user uid === $_currentUserUid\n\n\n");
  }

  Widget build(BuildContext context) {
    return StreamProvider<List<PostData>>.value(
      value: PostsDatabase().posts,
      child: Scaffold(
        backgroundColor: Colors.red[50],
        appBar: header(context, isAppTitle: true, title: "Community", appBarColor: Colors.pink[200]),
        body: Stack(
          children: <Widget>[
            // Container(
            //   height: MediaQuery.of(context).size.height,
            //   decoration: BoxDecoration(
            //     color: Colors.purple[50],
            //     image: DecorationImage(
            //       image: AssetImage("assets/images/undraw_pilates_gpdb.png"),
            //       alignment: Alignment.centerLeft,
            //     ),
            //   ),
            // ),
            SizedBox(
              height: 5.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Text(
                  "#wellness",
                  style: TextStyle(
                    color: Colors.blueGrey[500],
                    fontSize: 18.0,
                  ),
                  ),
                Text(
                  "|",
                  style: TextStyle(
                    color: Colors.blueGrey[500],
                    fontSize: 18.0,
                  ),
                  ),
                Text(
                  "#personalhygiene",
                  style: TextStyle(
                    color: Colors.blueGrey[500],
                    fontSize: 18.0,
                  ),
                  ),
                Text(
                  "|",
                  style: TextStyle(
                    color: Colors.blueGrey[500],
                    fontSize: 18.0,
                  ),
                  ),
                Text(
                  "#periods",
                  style: TextStyle(
                    color: Colors.blueGrey[500],
                    fontSize: 18.0,
                  ),
                  ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(8.0,22.0,8.0,8.0),
              child: Expanded(
                child: PostList()
                ),
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomRight,
                child: IconButton(
                    padding: EdgeInsets.only(right: 35.0, bottom: 30.0),
                    icon: Icon(Icons.add_circle, size: 55.0),
                    color: Colors.pink[300],
                    onPressed: () {
                      Navigator.pushNamed(context, '/newpost');
                    }),
              ),
            )
          ],
        ),
        bottomNavigationBar: BottomNavBar(),
      ),
    );
  }
}
