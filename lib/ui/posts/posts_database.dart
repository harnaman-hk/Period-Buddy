import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
import 'package:flowSure/ui/posts/post_model.dart';

class PostsDatabase {
  final CollectionReference postsCollection =
      Firestore.instance.collection("posts");
  final StorageReference postsStorageReference =
      FirebaseStorage.instance.ref().child("Post Images");

  savePostInfoToFirestore(
      {String postId, String userId, String url, String description}) {
    try {postsCollection
        // .document(userId)
        .document(postId)
        // .collection("usersPosts")
        .setData({
      "postId": postId,
      "ownerId": userId,
      "timestamp": Timestamp.now(),
      "likes": [],
      "comments": [],
      "description": description,
      "url": url,
    });} catch(e) {
      print("ERROR in savePostInfoToFirestore\n${e.toString()}");
    }
  }

  Future<String> uploadPhoto({String postId ,File imageFile}) async {
    StorageUploadTask imagestorageUploadTask =
        postsStorageReference.child("post_$postId.jpg").putFile(imageFile);
    StorageTaskSnapshot storageTaskSnapshot =
        await imagestorageUploadTask.onComplete;
    String url = await storageTaskSnapshot.ref.getDownloadURL();
    return url;
  }

  createNewPost(String postId, String userUid, String postDescription, File imageFile) async {
    if (imageFile != null){
    String downloadUrl = await uploadPhoto(postId: postId, imageFile: imageFile);
    savePostInfoToFirestore(postId: postId, userId: userUid ,url: downloadUrl, description: postDescription);
    } else {
    savePostInfoToFirestore(postId: postId, userId: userUid ,url: "", description: postDescription);
    }
    print("New Post Successful");
  }

  // post list from snapshot
  List<PostData> _postListFromSnapshot(QuerySnapshot snapshot) {
    return snapshot.documents.map((doc) {
      print("\n\nLikes list\n${doc.data["likes"]}\n");
      return PostData(
        postId: doc.data['postId'] ?? '',
        ownerId: doc.data['ownerId'] ?? '',
        // ownerName: doc.data['ownerName'] ?? 'owner name is null',
        // ownerUsername: doc.data['ownerName'] ?? 'unknownuser',
        time: doc.data['timestamp'],
        postDescription: doc.data['description'] ,
        imageUrl: doc.data['url'],
        likes: doc.data['likes'],
        comments: doc.data['comments'],
      );
    }).toList();
  }

  // get posts stream
  Stream<List<PostData>> get posts {
    return postsCollection.orderBy("timestamp", descending: true).snapshots()
       .map(_postListFromSnapshot);
    // return postsCollection.snapshots()
    // .map(_postListFromSnapshot);
  }

}
