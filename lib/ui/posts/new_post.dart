import 'package:flowSure/ui/user/auth.dart';
import 'package:flowSure/ui/layouts/header.dart';
import 'package:flowSure/ui/posts/posts_database.dart';
import 'package:flowSure/ui/user/user.dart';
import 'package:flowSure/ui/user/user_db.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;
import 'dart:io';

class NewPost extends StatefulWidget {
  // final String currentUserUid = AuthService().getCurrentUserUid().toString();

  @override
  _NewPostState createState() => _NewPostState();
}

class _NewPostState extends State<NewPost> {
  
  // UserData _currentUser = UserData();
  File file;
  final postTextController = TextEditingController();
  String postId = Uuid().v4();
  bool loading = false;
  captureImageWithCamera() async {
    Navigator.pop(context);
    PickedFile imageFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxHeight: 680,
      maxWidth: 970,
      imageQuality: 80,
    );
    setState(() {
      this.file = File(imageFile.path);
    });
  }

  pickImageFromGallery() async {
    Navigator.pop(context);
    PickedFile imageFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      imageQuality: 80,
    );
    setState(() {
      this.file = File(imageFile.path);
    });
  }

  uploadImage(mContext) {
    return showDialog(
      context: mContext,
      builder: (context) {
        return SimpleDialog(
          backgroundColor: Colors.purple[100],
          title: Text(
            "Select Image",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.deepPurple,
                fontWeight: FontWeight.bold,
                fontSize: 22.0),
          ),
          children: <Widget>[
            SimpleDialogOption(
              child: Text(
                "Capture Image with Camera",
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              ),
              onPressed: captureImageWithCamera,
            ),
            SimpleDialogOption(
              child: Text(
                "Choose from Gallery",
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              ),
              onPressed: pickImageFromGallery,
            ),
            SimpleDialogOption(
              child: Text(
                "Cancel",
                textAlign: TextAlign.center,
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
              ),
              onPressed: () => Navigator.pop(context),
            ),
          ],
        );
      },
    );
  }

  @override
  // void initState() {
  //   super.initState();
  //   initUser();
  // }

  // initUser() async {
  //   final FirebaseAuth _auth = FirebaseAuth.instance;
  //   String _currentUserUid;
  //   _currentUserUid = (await _auth.currentUser()).uid;
  //   // _currentUser = UserDatabase().userData
  //   print("current user uid === $_currentUserUid");
  // }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: header(context,
          isAppTitle: true,
          title: "New Post",
          disableBackButton: true,
          showBackButton: true,
          backButton: Icons.cancel),
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                Colors.pink[200],
                Colors.purple[200],
              ],
              begin: Alignment.topCenter,
            )),
          ),
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(16.0),
                child: Card(
                  child: Container(
                    color: Colors.white,
                    height: MediaQuery.of(context).size.height * 0.9,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(16.0),
                          // child: Text(
                          //   "Write something....",
                          //   style: TextStyle(
                          //     color: Colors.grey,
                          //   ),
                          // ),
                          child: Expanded(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: TextField(
                                controller: postTextController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: "Write something...."),
                                expands: false,
                                maxLines: null,
                              ),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            if (file == null)
                              RaisedButton.icon(
                                  onPressed: () => uploadImage(context),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  color: Colors.purple[100],
                                  icon: Icon(Icons.add_photo_alternate),
                                  label: Text(
                                    "Upload image",
                                    style: TextStyle(
                                        color: Colors.deepPurple,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  )),
                            if (file != null)
                              RaisedButton.icon(
                                  onPressed: () {
                                    setState(() {
                                      file = null;
                                    });
                                  },
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30.0),
                                  ),
                                  color: Colors.purple[100],
                                  icon: Icon(Icons.cancel),
                                  label: Text(
                                    "Remove image",
                                    style: TextStyle(
                                        color: Colors.deepPurple,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.0),
                                  )),
                            RaisedButton.icon(
                                onPressed: () async {
                                  setState(() {
                                    loading = true;
                                  });
                                  String userId = (await FirebaseAuth.instance
                                          .currentUser())
                                      .uid;
                                  await PostsDatabase().createNewPost(
                                      postId,
                                      userId,
                                      // _currentUser.profilename,
                                      // _currentUser.username,
                                      postTextController.text,
                                      file);
                                  setState(() {
                                    loading = true;
                                    postTextController.clear();
                                    file = null;
                                  });
                                  Navigator.popAndPushNamed(context, '/posts');
                                },
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30.0),
                                ),
                                color: Colors.purple[100],
                                icon: Transform.rotate(
                                    angle: -30 * math.pi / 180,
                                    child: Icon(
                                      Icons.send,
                                    )),
                                label: Text(
                                  "Post",
                                  style: TextStyle(
                                      color: Colors.deepPurple,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16.0),
                                )),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: (file != null),
                child: Card(
                  margin: EdgeInsets.only(left: 8.0, right: 8.0, top: 150.0),
                  child: Container(
                    height: 290.0,
                    // decoration: BoxDecoration(
                    //   image: DecorationImage(
                    //     image: FileImage(file),
                    //     fit: BoxFit.cover,
                    //   ),
                    // ),
                    child: file == null
                        ? SizedBox(height: 0.0, width: 0.0)
                        : Image.file(file, fit: BoxFit.cover),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
