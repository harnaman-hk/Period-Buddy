import 'package:cloud_firestore/cloud_firestore.dart';

class PostData {
  final String postId;
  final String ownerId;
  // final String ownerName;
  // final String ownerUsername;
  final Timestamp time;
  final List likes;
  final List comments;
  final String postDescription;
  final String imageUrl;

  PostData({ this.comments, this.imageUrl, this.likes, this.ownerId, this.postDescription, this.postId, this.time });
  // PostData({ this.imageUrl, this.ownerId, this.postDescription, this.postId, this.time, this.ownerName, this.ownerUsername });
}

class PostComment {
  final String userId;
  final String comment;
  PostComment({ this.comment, this.userId });
}