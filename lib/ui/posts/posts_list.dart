import 'package:flowSure/ui/posts/single_post_screen.dart';
import 'package:flowSure/ui/user/user.dart';
import 'package:flowSure/ui/user/user_db.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/posts/post_model.dart';
import 'package:provider/provider.dart';
import 'post_tile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class PostList extends StatefulWidget {
  @override
  _PostListState createState() => _PostListState();
}

class _PostListState extends State<PostList> {
  final CollectionReference postsCollection =
      Firestore.instance.collection("posts");
  @override
  Widget build(BuildContext context) {
    final posts = Provider.of<List<PostData>>(context) ?? [];
    return posts == []
        ? SizedBox(
            height: 0.0,
            width: 0.0,
          )
        : ListView.builder(
            itemCount: posts.length,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              return GestureDetector(
                child: PostTile(post: posts[index],),
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(builder: (context) => SinglePostScreen(post: posts[index]),)
                  // );
                },
                );
            },
          );
  }
}
