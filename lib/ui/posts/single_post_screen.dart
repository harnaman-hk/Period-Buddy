import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowSure/ui/layouts/constants.dart';
import 'package:flowSure/ui/posts/post_model.dart';
import 'package:flowSure/ui/user/user.dart';
import 'package:flowSure/ui/user/user_db.dart';
import 'package:flutter/material.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:provider/provider.dart';

class SinglePostScreen extends StatefulWidget {
  // final UserData user = UserData();
  final PostData post;
  SinglePostScreen({this.post});
  @override
  _SinglePostScreenState createState() => _SinglePostScreenState();
}

class _SinglePostScreenState extends State<SinglePostScreen> {
  final List<Color> _likeColours = [lightPastelPurple, Colors.red[600]];
  bool _liked = false;

  final firestoreInstance = Firestore.instance;
  String _uid = "";
  String _userProfileName = "";
  String _userUsername = "";
  String _photoUrl = "";
  int _likesCount = 0;
  List<PostComment> comments = [];
  controlUserLikes() {
    // if _liked is true i.e. user liked the post
    var doc = firestoreInstance.collection("posts").document(widget.post.postId);
    if(_liked) {
      doc.updateData({
        "likes" : FieldValue.arrayUnion(
          ["${widget.post.postId}"],
        )
      });
      setState(() {
        _likesCount += 1;
      });
    } else {
      doc.updateData({
        "likes" : FieldValue.arrayRemove(
          ["${widget.post.ownerId}"],          
        )
      });
      setState(() {
        _likesCount -= 1;
      });
    }
  }

    getComments() {
    Firestore.instance.collection("posts").document(widget.post.postId).get().then((value) {
      for(int i = 0; i < value.data["comments"].length; i+=2){
        comments.add(PostComment(userId: value.data["comments"][i], comment: value.data["comments"][i+1]));
      }
    });
  }

  String getUserName(String uid) {
    String userprofilename = "";
    firestoreInstance.collection("posts").document(uid).get().then((value) {
      userprofilename = value.data["profilename"];
    });
    return userprofilename;
  }

  @override
  void initState() {
    super.initState();
    initUserDetails();
  }

  initUserDetails() async {
    firestoreInstance
        .collection("users")
        .document(widget.post.ownerId)
        .get()
        .then((value) {
      print("post tile");
      print(value.data);

      setState(() {
        _uid = widget.post.ownerId;
        _userProfileName = value["profilename"];
        _userUsername = value["username"];
        _photoUrl = value["photourl"];
        _likesCount = widget.post.likes.length;
      });
    });
    getComments();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.post.likes.contains(widget.post.ownerId)) {
      _liked = true;
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: GradientCard(
        elevation: 2.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        gradient: LinearGradient(colors: [
          Colors.pink[50],
          lightPastelPink1,
        ], begin: Alignment.topCenter),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(16.0, 16.0, 8.0, 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        height: 40.0,
                        width: 40.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: NetworkImage(_photoUrl),
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Text(
                        "$_userProfileName",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20.0),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "@$_userUsername",
                      style: TextStyle(
                        color: Colors.blueGrey[700],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 8.0),
              child: Text(
                "${widget.post.postDescription}",
                style: TextStyle(
                  fontSize: 18.0,
                ),
              ),
            ),
            if (widget.post.imageUrl != "")
              Container(
                height: MediaQuery.of(context).size.height * 0.3,
                alignment: Alignment.center,
                child: Image.network(
                  "${widget.post.imageUrl}",
                  fit: BoxFit.fill,
                  width: MediaQuery.of(context).size.width * 0.95,
                ),
              ),
            SizedBox(
              height: 5.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton.icon(
                    onPressed: () {
                      setState(() {
                        _liked = !_liked;
                      });
                      controlUserLikes();
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    icon: Icon(
                      Icons.favorite,
                      color: _liked ? _likeColours[1] : _likeColours[0],
                      size: 25.0,
                    ),
                    color: Colors.pink[200],
                    elevation: 0.0,
                    label: Text("$_likesCount")),
                RaisedButton.icon(
                    onPressed: () {},
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    icon: Icon(Icons.comment),
                    color: Colors.pink[200],
                    elevation: 0.0,
                    label: Text("${widget.post.comments.length}"))
              ],
            ),
            SizedBox(
              height: 8.0,
            ),
            ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: comments.length,
              itemBuilder: (context, index) {
                return Container(
                  child: Column(
                    children: <Widget> [
                      Text(
                        "${getUserName(comments[index].userId)}"
                      ),
                      Text(
                        "${comments[index].comment}"
                      )
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
