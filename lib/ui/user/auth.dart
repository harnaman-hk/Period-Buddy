import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowSure/ui/user/user_db.dart';
import 'user.dart';
class AuthService {

  final FirebaseAuth _auth = FirebaseAuth.instance;

  User _userFromFirebaseUser(FirebaseUser user) {
    return user != null ? User(uid: user.uid) : null;
  }

  Stream<User> get user {
    return _auth.onAuthStateChanged
      .map(_userFromFirebaseUser);
  }

  Future createNewUser(String username, String password) async {
    try {
      String dummyEmail = username + "@gmail.com";
      // print(dummyEmail);
      AuthResult result = await _auth.createUserWithEmailAndPassword(email: dummyEmail, password: password);
      FirebaseUser user = result.user;

      await UserDatabase(uid: user.uid).updateUserData(uid: user.uid, username: username, profilename: username);
      return _userFromFirebaseUser(user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  }
  Future signInWithUsernameAndPassword(String username, String password) async {
    try {
      String dummyEmail = username + "@gmail.com";
      AuthResult result = await _auth.signInWithEmailAndPassword(email: dummyEmail, password: password);
      return _userFromFirebaseUser(result.user);
    } catch(e) {
      print(e.toString());
      return null;
    }
  } 
  Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future<String> getCurrentUserUid() async {
    try {
      FirebaseUser user = await _auth.currentUser();
      print(_userFromFirebaseUser(user).uid);
      return _userFromFirebaseUser(user).uid;
    } catch(e) {
      print("ERROR: Couldn't get current user UID\n${e.toString()}");
      return null;
    }
  }

}