

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flowSure/ui/user/user.dart';

class UserDatabase {

  final String uid;
  UserDatabase({ this.uid });

  final CollectionReference userCollection = Firestore.instance.collection('users');
  final StorageReference userPictureReference = FirebaseStorage.instance.ref().child("User Pictures");

  Future updateUserData({String uid ,String username, String profilename, File imageFile}) async {
    String url = "https://firebasestorage.googleapis.com/v0/b/flowsure-4b3c6.appspot.com/o/defaults%2Fhappygirl.jpeg?alt=media&token=3fe847a9-465f-43db-a64f-7148a9eae456";
    if(imageFile != null) {
      StorageUploadTask imageStorageTask = userPictureReference.child("user$uid.jpg").putFile(imageFile);
      StorageTaskSnapshot storageTaskSnapshot = await imageStorageTask.onComplete;
      url = await storageTaskSnapshot.ref.getDownloadURL();
    }
    return await userCollection
        .document(uid)
        .setData({
          'uid': uid,
          'username': username,
          'profilename': profilename,
          'photourl': url,
        })
        .then((value) => {
              userCollection
                  .document(uid)
                  .collection("events")
                  .add({"Period": {}})
            })
        .then((value) =>
            {userCollection.document(uid).collection("events1").add({})});
  }

  // UserData getUserData() async{
  //   return userCollection.document(uid).get().then((doc) {
  //     if (doc.exists) {
  //       print("Document retreived for user id $uid");
  //       print("doc.data");
  //       return UserData(
  //         uid: doc.data["uid"] ?? "",
  //         profilename: doc.data["profilename"] ?? "",
  //         username: doc.data["username"] ?? "",
  //         photourl: doc.data["photourl"] ?? ""
  //       );
  //     }
  //   });
  // }

  UserData _userDataFromSnapshot(DocumentSnapshot snapshot) {
    print(snapshot.data);
    return UserData(
      uid: uid ?? "",
      profilename: snapshot.data['profilename'] ?? "",
      username: snapshot.data['username'] ?? "",
      photourl: snapshot.data['photourl'] ?? "",
    );
  }

  Stream<UserData> get userData {
    return userCollection.document(uid).snapshots().map(_userDataFromSnapshot);
  }

}