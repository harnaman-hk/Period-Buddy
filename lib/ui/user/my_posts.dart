import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowSure/ui/layouts/header.dart';
import 'package:flowSure/ui/posts/post_model.dart';
import 'package:flowSure/ui/posts/post_tile.dart';
import 'package:flutter/material.dart';

class MyPosts extends StatefulWidget {
  @override
  _MyPostsState createState() => _MyPostsState();
}

class _MyPostsState extends State<MyPosts> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser user;
  String _uid = "";
  List<PostData> posts = [];
  final firestoreInstance = Firestore.instance;
  @override
  void initState() {
    super.initState();
    initUser();
  }
  initUser() async{
    user = await _auth.currentUser();
    _uid = user.uid;
    var result = await firestoreInstance.collection("posts").where("ownderId", isEqualTo: _uid).getDocuments();
    result.documents.forEach((element) {
      posts.add(PostData(
        postId: element.data["postId"],
        ownerId: element.data["ownerId"],
        likes: element.data["likes"],
        comments: element.data["comments"],
        postDescription: element.data["description"],
        time: element.data["timestamp"],
        imageUrl: element.data["url"]
      ));
    });
  }
  Widget build(BuildContext context) {
    return posts == [] ? Scaffold(body: SafeArea(child: Center(child: Text("No Posts"),),),):Scaffold(
      appBar: header(context,
                  isAppTitle: true,
                  title: "Community",
                  appBarColor: Colors.pink[200]),
      body: SafeArea(
        child: ListView.builder(
          itemCount: posts.length,
          itemBuilder: (context, index) {
            return PostTile(post: posts[index]);
          },
        ),
      ),
    );
  }
}