import 'package:flowSure/ui/layouts/navbar.dart';
import 'package:flowSure/ui/user/auth.dart';
import 'package:flutter/material.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _authService = AuthService();

  final firestoreInstance = Firestore.instance;
  String _uid = "";

  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser user;

  Widget buildWorkList(
      BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
    if (snapshot.hasData) {
      return ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: snapshot.data.documents.length,
        itemBuilder: (context, index) {
          DocumentSnapshot post = snapshot.data.documents[index];
          // String postId = post.postId;

          return Card(
              child: Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.pink[100], Colors.white],
                      begin: Alignment.bottomRight,
                      end: Alignment.topLeft,
                    ),
                  ),
                  child: ListTile(
                    // Access the fields as defined in FireStore
                    title: Row(
                      children: <Widget>[
                        Icon(
                          Icons.favorite,
                          color: Colors.red[300],
                        ),
                        SizedBox(
                          width:
                              5, // here put the desired space between the icon and the text
                        ),
                        SizedBox(height: 10),
                      ],
                    ),
                    subtitle: Text(
                      '${post.data["description"]}',
                      style: TextStyle(
                        fontFamily: 'Lora',
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  )));
        },
      );
    } else if (snapshot.connectionState == ConnectionState.done &&
        !snapshot.hasData) {
      // Handle no data
      return Center(
        child: Text("No posts found."),
      );
    } else {
      // Still loading
      return CircularProgressIndicator();
    }
  }

  @override
  void initState() {
    super.initState();
    initUser();
  }

  initUser() async {
    user = await _auth.currentUser();
    firestoreInstance
        .collection("posts")
        .document(user.uid)
        .get()
        .then((value) {
      print(value.data);

      setState(() {
        _uid = user.uid;
      });
    });
    // setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // print('from Home page');
    // print('$name, $address, $mainarea, $phoneno');
    return Container(
        child: Scaffold(
          bottomNavigationBar: BottomNavBar(),
            backgroundColor: Colors.blue[50],
            appBar: AppBar(
              backgroundColor: Colors.pink[200],
              title: Center(
                child: Text(
                  'Profile',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "PlayfairDisplay",
                      fontSize: 30,
                      letterSpacing: 2.0),
                ),
              ),
            ),
            body: SafeArea(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(10),
                    width: MediaQuery.of(context).size.width,
                    height: 550,
                    child: FutureBuilder(
                        builder: buildWorkList,
                        future: Firestore.instance
                            .collection('posts')
                            .where("ownerId", isEqualTo: _uid)
                            .getDocuments()),
                  ),
                  // SizedBox(height: 10),
                ],
              ),
            )));
  }
}
