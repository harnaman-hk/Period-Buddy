class User {

  final String uid;
  // final String username;

  User({ this.uid });
  
}

class UserData {
  final String uid;
  final String profilename;
  final String username;
  final String photourl;

  UserData({ this.uid, this.profilename, this.username ,this.photourl });
}