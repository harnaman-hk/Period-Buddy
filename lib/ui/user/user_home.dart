import 'package:firebase_auth/firebase_auth.dart';
import 'package:flowSure/ui/layouts/constants.dart';
import 'package:flowSure/ui/layouts/header.dart';
import 'package:flowSure/ui/layouts/loading.dart';
import 'package:flowSure/ui/layouts/navbar.dart';
import 'package:flowSure/ui/posts/post_model.dart';
import 'package:flowSure/ui/posts/posts_database.dart';
import 'package:flowSure/ui/user/auth.dart';
import 'package:flowSure/ui/user/user.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/user/user_db.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'dart:io';
import 'package:uuid/uuid.dart';
import 'package:image_picker/image_picker.dart';

class UserMenu extends StatefulWidget {
  @override
  _UserMenuState createState() => _UserMenuState();
}

class _UserMenuState extends State<UserMenu> {
  bool loading = false;

  String _username = "";
  String _profilename = "";

  final FirebaseAuth _auth = FirebaseAuth.instance;
  FirebaseUser user;
  final firestoreInstance = Firestore.instance;
  String _uid = "";
  String _currentName = "";
  String _currentUsername = "";
  String _pictureUrl = "";
  File file;
  String postId = Uuid().v4();

  captureImageWithCamera() async {
    Navigator.pop(context);
    PickedFile imageFile = await ImagePicker().getImage(
      source: ImageSource.camera,
      maxHeight: 680,
      maxWidth: 970,
      imageQuality: 80,
    );
    setState(() {
      this.file = File(imageFile.path);
    });
  }

  pickImageFromGallery() async {
    Navigator.pop(context);
    PickedFile imageFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      imageQuality: 80,
    );
    setState(() {
      this.file = File(imageFile.path);
    });
  }

  @override
  void initState() {
    super.initState();
    initUser();
  }

  initUser() async {
    user = await _auth.currentUser();
    firestoreInstance
        .collection("users")
        .document(user.uid)
        .get()
        .then((value) {
      print("retreival of user");
      print(value.data);

      setState(() {
        _uid = user.uid;
        _currentName = value["profilename"];
        _currentUsername = value["username"];
        _pictureUrl = value["photourl"];
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    print("\n\n$_uid\n\n");
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.pink[50],
            appBar: AppBar(
              backgroundColor: Colors.pink[200],
              title: Center(
                child: Text(
                  'Profile',
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "PlayfairDisplay",
                      fontSize: 30,
                      letterSpacing: 2.0),
                ),
              ),
            ),
            body: Stack(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height * 0.40,
                  // color: Colors.pink[200],
                  decoration: BoxDecoration(
                    image: const DecorationImage(
                      image: AssetImage('assets/userbg.gif'),
                      fit: BoxFit.fill,
                    ),
                    color: Colors.white,
                    // gradient: LinearGradient(
                    //     colors: [Colors.pink[200], Colors.pink[100]],
                    //     begin: Alignment.topCenter,
                    //     end: Alignment.bottomLeft),
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(50.0),
                        bottomRight: Radius.circular(50.0)),
                  ),
                ),
                SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        height: 20.0,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircleAvatar(
                            radius: 85,
                            backgroundColor: Colors.pink[200],
                            child: CircleAvatar(
                              radius: 80,
                              backgroundImage: NetworkImage("$_pictureUrl"),
                            ),
                          )
                          // CircleAvatar(
                          //   radius: 80.0,
                          //   backgroundColor: Colors.pink[100],
                          //   backgroundImage:
                          //
                          // ),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              bottom: 10, top: 10, left: 5, right: 5),
                          decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(
                                color: Colors.blue[50],
                                blurRadius: 15.0,
                              ),
                            ],
                          ),
                          child: Card(
                              //height: MediaQuery.of(context).size.height * 20,

                              elevation: 5.0,
                              color: Colors.yellow[50],
                              child: Column(children: [
                                SizedBox(
                                  height: 5.0,
                                ),
                                Container(
                                  padding: EdgeInsets.all(5),
                                  child: Text(
                                    "$_currentName",
                                    style: TextStyle(
                                      fontSize: 27.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 4.0,
                                ),
                                Container(
                                  padding: EdgeInsets.all(5.0),
                                  child: Text(
                                    "@" + "$_currentUsername",
                                    style: TextStyle(
                                      fontSize: 15.0,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                              ]))),
                      Expanded(
                        child: ListView(
                          scrollDirection: Axis.vertical,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                Alert(
                                  context: context,
                                  title: "Edit Profile",
                                  style: AlertStyle(
                                    animationType: AnimationType.fromBottom,
                                    animationDuration:
                                        Duration(milliseconds: 400),
                                    backgroundColor: Colors.blue[50],
                                  ),
                                  content: Column(
                                    children: <Widget>[
                                      CircleAvatar(
                                        radius: 80,
                                        backgroundImage:
                                            file == null ? NetworkImage("$_pictureUrl") : Image.file(file, fit: BoxFit.cover),
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                        IconButton(icon: Icon(Icons.camera_alt), onPressed: (){
                                          captureImageWithCamera();
                                        },),
                                        SizedBox(width: 10.0,),                                          
                                        IconButton(icon: Icon(Icons.photo_album), onPressed: (){
                                          pickImageFromGallery();
                                        },),
                                        ],
                                      ),
                                      TextFormField(
                                          initialValue: _currentName,
                                          onChanged: (val) {
                                            setState(() {
                                              _profilename = val;
                                            });
                                          },
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.pink[100])),
                                            border: OutlineInputBorder(),
                                            labelText: "Display Name",
                                            labelStyle: TextStyle(
                                                color: Colors.pink[200],
                                                fontSize: 18.0),
                                          )),
                                      TextFormField(
                                          initialValue: _currentUsername,
                                          onChanged: (val) {
                                            setState(() {
                                              _username = val;
                                            });
                                          },
                                          decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.grey),
                                            ),
                                            focusedBorder: UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.pink[100])),
                                            border: OutlineInputBorder(),
                                            labelText: "Username",
                                            labelStyle: TextStyle(
                                                color: Colors.pink[200],
                                                fontSize: 18.0),
                                          )),
                                    ],
                                  ),
                                  buttons: [
                                    DialogButton(
                                      color: Colors.pink[200],
                                      child: Text(
                                        "Save Changes",
                                      ),
                                      onPressed: () async {
                                        _username = _username == ""
                                            ? _currentUsername
                                            : _username;
                                        _profilename = _profilename == ""
                                            ? _currentName
                                            : _profilename;
                                        await UserDatabase(uid: _uid)
                                            .updateUserData(
                                                uid: _uid,
                                                username: _username,
                                                profilename: _profilename,
                                                imageFile: file);
                                        Navigator.pop(context);
                                        Navigator.pushReplacementNamed(context, '/user');
                                      },
                                    ),
                                  ],
                                ).show();
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    // left: BorderSide(
                                    //   //                   <--- left side
                                    //   color: Colors.blueGrey,
                                    //   width: 3.0,
                                    // ),
                                    bottom: BorderSide(
                                      //                    <--- top side
                                      color: Colors.blue[200],
                                      width: 6.0,
                                    ),
                                  ),
                                ),
                                child: Card(
                                  borderOnForeground: false,
                                  color: Colors.blue[50],
                                  elevation: 5.0,
                                  child: ListTile(
                                    contentPadding: EdgeInsets.only(
                                        left: 120.0, right: 140.0),
                                    leading: Icon(
                                      Icons.mode_edit,
                                      size: 35.0,
                                    ),
                                    title: Text(
                                      "Edit Profile",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.pushNamed(context, '/home');
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    // left: BorderSide(
                                    //   //                   <--- left side
                                    //   color: Colors.blueGrey,
                                    //   width: 3.0,
                                    // ),
                                    bottom: BorderSide(
                                      //                    <--- top side
                                      color: Colors.blue[200],
                                      width: 6.0,
                                    ),
                                  ),
                                ),
                                //width: MediaQuery.of(context).size.width * 50,
                                child: Card(
                                  borderOnForeground: false,
                                  color: Colors.blue[50],
                                  elevation: 5.0,
                                  child: ListTile(
                                    contentPadding: EdgeInsets.only(
                                        left: 120.0, right: 140.0),
                                    leading: Icon(
                                      Icons.speaker_notes,
                                      size: 35.0,
                                    ),
                                    title: Text(
                                      "My Posts",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(height: 10),
                            InkWell(
                              onTap: () async {
                                final FirebaseAuth _auth =
                                    FirebaseAuth.instance;
                                _auth.signOut();
                                Navigator.pushReplacementNamed(context, '/');
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    // left: BorderSide(
                                    //   //                   <--- left side
                                    //   color: Colors.blueGrey,
                                    //   width: 3.0,
                                    // ),
                                    bottom: BorderSide(
                                      //                    <--- top side
                                      color: Colors.green[200],
                                      width: 6.0,
                                    ),
                                  ),
                                ),
                                child: Card(
                                  borderOnForeground: false,
                                  color: Colors.blue[50],
                                  elevation: 5.0,
                                  child: ListTile(
                                    contentPadding: EdgeInsets.only(
                                        left: 120.0, right: 140.0),
                                    leading: Icon(
                                      Icons.arrow_back,
                                      size: 35.0,
                                    ),
                                    title: Text(
                                      "Sign Out",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
            bottomNavigationBar: BottomNavBar(),
          );
  }
  // Widget build(BuildContext context) {
  //   User currentUser = Provider.of<User>(context);
  //   return StreamBuilder<UserData>(
  //     stream: UserDatabase(uid: currentUser.uid).userData,
  //     builder: (context, snapshot) {
  //       if (snapshot.hasData) {
  //         UserData userData = snapshot.data;
  //         return Scaffold(
  //           backgroundColor: Colors.pink[50],
  //           appBar: header(context,
  //               isAppTitle: true,
  //               title: "Profile",
  //               appBarColor: Colors.pink[200]),
  //           body: Stack(
  //             children: <Widget>[
  //               Container(
  //                 height: MediaQuery.of(context).size.height * 0.40,
  //                 // color: Colors.pink[200],
  //                 decoration: BoxDecoration(
  //                   color: Colors.pink[200],
  //                   gradient: LinearGradient(
  //                       colors: [Colors.pink[200], Colors.pink[100]],
  //                       begin: Alignment.topCenter,
  //                       end: Alignment.bottomLeft),
  //                   borderRadius: BorderRadius.only(
  //                       bottomLeft: Radius.circular(50.0),
  //                       bottomRight: Radius.circular(50.0)),
  //                 ),
  //               ),
  //               SafeArea(
  //                 child: Column(
  //                   mainAxisAlignment: MainAxisAlignment.start,
  //                   children: <Widget>[
  //                     SizedBox(
  //                       height: 20.0,
  //                     ),
  //                     Column(
  //                       mainAxisAlignment: MainAxisAlignment.center,
  //                       crossAxisAlignment: CrossAxisAlignment.center,
  //                       children: <Widget>[
  //                         CircleAvatar(
  //                           radius: 80.0,
  //                           backgroundColor: Colors.pink[100],
  //                           backgroundImage: NetworkImage("${userData.photourl}"),
  //                           ),
  //                       ],
  //                     ),
  //                     SizedBox(
  //                       height: 10.0,
  //                     ),
  //                     Text(
  //                       "${userData.profilename}",
  //                       style: TextStyle(
  //                         fontSize: 27.0,
  //                         fontWeight: FontWeight.bold,
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       height: 4.0,
  //                     ),
  //                     Text(
  //                       "@" + "${userData.username}",
  //                       style: TextStyle(
  //                         fontSize: 15.0,
  //                       ),
  //                     ),
  //                     SizedBox(
  //                       height: 30.0,
  //                     ),
  //                     Expanded(
  //                       child: ListView(
  //                         scrollDirection: Axis.vertical,
  //                         children: <Widget>[
  //                           InkWell(
  //                             onTap: () {
  //                               Alert(
  //                                 context: context,
  //                                 title: "Edit Profile",
  //                                 style: AlertStyle(
  //                                   animationType: AnimationType.fromBottom,
  //                                   animationDuration: Duration(milliseconds: 400),
  //                                   backgroundColor: Colors.pink[50],
  //                                 ),
  //                                 content: Column(
  //                                   children: <Widget>[
  //                                     TextFormField(
  //                                       initialValue: userData.profilename,
  //                                       onChanged: (val) {
  //                                         setState(() {
  //                                           _profilename = val;
  //                                         });
  //                                       },
  //                                       decoration: InputDecoration(
  //                                         enabledBorder: UnderlineInputBorder(
  //                                           borderSide:
  //                                               BorderSide(color: Colors.grey),
  //                                         ),
  //                                         focusedBorder: UnderlineInputBorder(
  //                                             borderSide: BorderSide(
  //                                                 color: Colors.pink[100])),
  //                                         border: OutlineInputBorder(),
  //                                         labelText: "Display Name",
  //                                         labelStyle: TextStyle(
  //                                             color: Colors.pink[200],
  //                                             fontSize: 18.0),
  //                                       )
  //                                     ),
  //                                     TextFormField(
  //                                       initialValue: userData.username,
  //                                       onChanged: (val) {
  //                                         setState(() {
  //                                           _username = val;
  //                                         });
  //                                       },
  //                                       decoration: InputDecoration(
  //                                         enabledBorder: UnderlineInputBorder(
  //                                           borderSide:
  //                                               BorderSide(color: Colors.grey),
  //                                         ),
  //                                         focusedBorder: UnderlineInputBorder(
  //                                             borderSide: BorderSide(
  //                                                 color: Colors.pink[100])),
  //                                         border: OutlineInputBorder(),
  //                                         labelText: "Username",
  //                                         labelStyle: TextStyle(
  //                                             color: Colors.pink[200],
  //                                             fontSize: 18.0),
  //                                       )
  //                                     ),
  //                                   ],
  //                                 ),
  //                                 buttons: [
  //                                   DialogButton(
  //                                     child: Text(
  //                                       "Save Changes",
  //                                     ),
  //                                     onPressed: () async {
  //                                       _username = _username == "" ? userData.username : _username;
  //                                       _profilename = _profilename == "" ? userData.profilename : _profilename;
  //                                       await UserDatabase(uid: userData.uid).updateUserData(uid: userData.uid, username: _username, profilename: _profilename);
  //                                       Navigator.pop(context);
  //                                     },
  //                                   ),
  //                                 ],
  //                               ).show();
  //                             },
  //                             child: Card(
  //                               borderOnForeground: false,
  //                               color: Colors.pink[50],
  //                               elevation: 0.0,
  //                               child: ListTile(
  //                                 contentPadding: EdgeInsets.only(
  //                                     left: 120.0, right: 140.0),
  //                                 leading: Icon(
  //                                   Icons.mode_edit,
  //                                   size: 35.0,
  //                                 ),
  //                                 title: Text(
  //                                   "Edit Profile",
  //                                   textAlign: TextAlign.center,
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                           SizedBox(
  //                             height: 10.0,
  //                           ),
  //                           InkWell(
  //                             child: Card(
  //                               borderOnForeground: false,
  //                               color: Colors.pink[50],
  //                               elevation: 0.0,
  //                               child: ListTile(
  //                                 contentPadding: EdgeInsets.only(
  //                                     left: 120.0, right: 140.0),
  //                                 leading: Icon(
  //                                   Icons.calendar_today,
  //                                   size: 35.0,
  //                                 ),
  //                                 title: Text(
  //                                   "Period Logs",
  //                                   textAlign: TextAlign.center,
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                           // SizedBox(
  //                           //   height: 10.0,
  //                           // ),
  //                           InkWell(
  //                             child: Card(
  //                               borderOnForeground: false,
  //                               color: Colors.pink[50],
  //                               elevation: 0.0,
  //                               child: ListTile(
  //                                 contentPadding: EdgeInsets.only(
  //                                     left: 120.0, right: 140.0),
  //                                 leading: Icon(
  //                                   Icons.speaker_notes,
  //                                   size: 35.0,
  //                                 ),
  //                                 title: Text(
  //                                   "My Posts",
  //                                   textAlign: TextAlign.center,
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                           InkWell(
  //                             onTap: () async{
  //                               final FirebaseAuth _auth = FirebaseAuth.instance;
  //                               _auth.signOut();
  //                               Navigator.pushReplacementNamed(context, '/');
  //                             },
  //                             child: Card(
  //                               borderOnForeground: false,
  //                               color: Colors.pink[50],
  //                               elevation: 0.0,
  //                               child: ListTile(
  //                                 contentPadding: EdgeInsets.only(
  //                                     left: 120.0, right: 140.0),
  //                                 leading: Icon(
  //                                   Icons.arrow_back,
  //                                   size: 35.0,
  //                                 ),
  //                                 title: Text(
  //                                   "Sign Out",
  //                                   textAlign: TextAlign.center,
  //                                 ),
  //                               ),
  //                             ),
  //                           ),
  //                         ],
  //                       ),
  //                     ),
  //                   ],
  //                 ),
  //               ),
  //             ],
  //           ),
  //           bottomNavigationBar: BottomNavBar(),
  //         );
  //       } else {
  //         return Loading();
  //       }
  //     },
  //   );
  // }
}
