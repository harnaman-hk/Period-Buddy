import 'package:flowSure/ui/calendar/calendar_home.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flowSure/ui/authenticate/authenticate.dart';
import 'package:flowSure/ui/user/user.dart';

class Wrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    if (user == null){
      return Authenticate();
    } else {
      return Calendar();
    }
  }
}