import 'package:flutter/material.dart';

AppBar header(context,
    {bool isAppTitle = false, String title, disableBackButton = false, showBackButton = false, backButton = Icons.arrow_back_ios, Color appBarColor}) {
  return AppBar(
    leading: !showBackButton ? SizedBox(height: 0.0, width: 0.0,) : IconButton(
        icon: Icon(backButton, color: Colors.grey[600],),
        onPressed: () => Navigator.of(context).pop(),
      ),
    iconTheme: IconThemeData(
      color: Colors.pink,
    ),
    automaticallyImplyLeading: disableBackButton ? false : true,
    title: Text(
      isAppTitle ? title : "FlowSure",
      style: TextStyle(
        color: Colors.pink[50],
        fontFamily: isAppTitle ? "Lobster" : "",
        fontSize: isAppTitle ? 40.0 : 22.0,
      ),
      overflow: TextOverflow.ellipsis,
    ),
    centerTitle: true,
    backgroundColor: appBarColor == null ? Colors.pink[200] : appBarColor,
    elevation: 0.0,
  );
}
