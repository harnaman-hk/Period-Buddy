import 'package:flutter/material.dart';

const veryLightPastelPink = Color(0xFFfff0f5);
const lightPastelPink = Color.fromRGBO(255, 209, 220, 1.0);
const lightPastelPink1 = Color(0xFFffd7e4);
const userBackgroundPink = Color.fromRGBO(255, 158, 181, 1.0);
const userHomeAppbar = Color(0xFFde9dac);
const lightPastelPurple = Color(0xFFece6ff);