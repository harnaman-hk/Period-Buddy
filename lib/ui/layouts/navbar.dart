import 'package:flutter/material.dart';

class BottomNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, bottom: 30),
      height: 70.0,
      decoration: BoxDecoration(
        color: Color.fromRGBO(240, 132, 182, 1),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(40.0),
          topRight: Radius.circular(40.0),
          // bottomLeft: Radius.circular(40.0),
          // bottomRight: Radius.circular(40.0),

        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          IconButton(
            color: Colors.black,
            icon: Icon(
              Icons.question_answer,
              size: 30.0,
            ),
            onPressed: () {
              Navigator.pushReplacementNamed(context, '/home_screen');
            },
          ),
          IconButton(
              color: Colors.black,
              icon: Icon(
                Icons.event_note,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/');
              }),
          IconButton(
              color: Colors.black,
              icon: Icon(
                Icons.fastfood,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/health');
              }),
          IconButton(
              color: Colors.black,
              icon: Icon(
                Icons.person,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/user');
              }),
          IconButton(
              icon: Icon(
                Icons.fitness_center,
                size: 40.0,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/exercise_section');
              }),
        ],
      ),
    );
  }
}
