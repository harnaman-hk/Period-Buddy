import 'package:cloud_firestore/cloud_firestore.dart';

class Estimateperiod {
  int cycleSize = 28;

  Map<String, List> createArray1(Map m, Map months) {
//    print(m);
    List<DateTime> list = [];
    m.forEach((key, value) {
//      print('$key');
      List l = value;
      l.forEach((element) {
        if (element.runtimeType == Timestamp) {
          Timestamp time = element;
          DateTime p =
              DateTime.fromMicrosecondsSinceEpoch(time.microsecondsSinceEpoch);
          element = p;
        }
        list.add(element);
      });
    });
    list.sort((b, a) => a.compareTo(b));
//    print(list);
    int sum = 0;
    for (var i = 0; i < list.length - 1; i++) {
      sum += list[i].difference(list[i + 1]).inDays;
    }
//    if (list.length > 3) cycleSize = ((sum / list.length - 1).round());
    DateTime latest = list.first;
//    print('$cycleSize,$latest,$list');
    Map<String, List> estimate = {};
    for (var i = 0; i < 12; i++) {
      DateTime nw = latest.add(Duration(
        days: cycleSize,
      ));
      String name =
          (months[nw.month]).toString().substring(0, 3).toLowerCase() +
              '-${nw.year}';
//      print('$name and $nw');
      if (estimate[name] != null)
        estimate[name].add(nw);
      else {
        estimate[name] = [nw];
      }
      latest = nw;
    }
//    print('cycle is $cycleSize, estimate is $estimate,period is $m');

    return estimate;
  }

  int calculate(Map m) {
    List<DateTime> list = [];
    m.forEach((key, value) {
//      print('$key');
      List l = value;
      l.forEach((element) {
        list.add(element);
      });
    });
    list.sort((b, a) => a.compareTo(b));
    return (list.length > 1 ? list[0].difference(list[1]).inDays : 28);
  }

  List createEvents(Map a1, Map b1) {
//    print("a1 map $a1");
//    print("b1 map $b1");
    Map<String, List> a = a1;
    Map<String, List> b = b1;
    List l = [];
    a.forEach((key, value) {
      List li = value;
      li.forEach((element) {
        l.add(element);
      });
    });
    b.forEach((key, value) {
      List li = value;
      li.forEach((element) {
        l.add(element);
      });
    });
    l.sort((b, a) => a.compareTo(b));
    return l;
  }
}
