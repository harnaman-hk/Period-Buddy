import 'package:flowSure/ui/calendar/estimatedperiod.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/layouts/navbar.dart';
import 'package:table_calendar/table_calendar.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class CalendarMonth extends StatefulWidget {
  @override
  _CalendarMonthState createState() => _CalendarMonthState();
}

class _CalendarMonthState extends State<CalendarMonth> {
  final _auth = FirebaseAuth.instance;
  String _currentUserUsername = "";
  final _firestore = Firestore.instance;
  bool _progressController = true;
  CalendarController _controller;
  Map<DateTime, List<dynamic>> _events;
  Map<DateTime, List<dynamic>> _holidays;
  Map<String, List> eventsDb = {};
  List<dynamic> _selectedEvents;
  TextEditingController _eventController;
  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
    _eventController = TextEditingController();
    _events = {};
    _holidays = {};
    _selectedEvents = [];

    initUser();
  }

  initUser() async {
    FirebaseUser _user = await _auth.currentUser();
    _firestore.collection("users").document(_user.uid).get().then((value) {
      print(value.data);
      _currentUserUsername = value["username"];
      datesStream();
      periodData();
    });
  }

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  datesStream() async {
//    print('enterd');
    _firestore
        .collection("users")
        .where('username', isEqualTo: _currentUserUsername)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
//        print(result.data);
        _firestore
            .collection("users")
            .document(result.documentID)
            .collection("events1")
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
            result.data.forEach((key, value) {
              String s = key;
              List k = s.split('-');
              DateTime time = new DateTime(
                  int.parse(k[2]), int.parse(k[1]), int.parse(k[0]));
              _events[time] = value;
            });
          });
        });
      });
    });
  }

  Map<int, String> monthsInYear = {
    1: "January",
    2: "February",
    3: "March",
    4: 'April',
    5: 'May',
    6: 'June',
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
  };

  periodData() async {
    _firestore
        .collection("users")
        .where('username', isEqualTo: '$_currentUserUsername')
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        _firestore
            .collection("users")
            .document(result.documentID)
            .collection("events")
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
            if (result.data['Period'] != null) {
              Map map = (result.data['Period']);
              map.forEach((key, value) {
                List list = value;
                list.forEach((element) {
                  Timestamp time = element;
                  DateTime p = DateTime.fromMicrosecondsSinceEpoch(
                      time.microsecondsSinceEpoch);
                  if (_holidays[p] != null)
                    _holidays[p].add('Day 1');
                  else
                    _holidays[p] = ['Day 1'];
                  for (var i = 2; i <= 5; i++) {
                    p = p.add(Duration(days: 1));
                    if (_holidays[p] != null)
                      _holidays[p].add('Day $i');
                    else
                      _holidays[p] = ['Day $i'];
                  }
                });
              });
              Estimateperiod estimateperiod = Estimateperiod();
              Map<String, List> m =
                  estimateperiod.createArray1(map, monthsInYear);
              m.forEach((key, value) {
                List k = value;
                k.forEach((element) {
                  if (_holidays[element] != null)
                    _holidays[element].add('Day 1');
                  else
                    _holidays[element] = ['Day 1'];
                  for (var i = 2; i <= 5; i++) {
                    element = element.add(Duration(days: 1));
                    if (_holidays[element] != null)
                      _holidays[element].add('Day $i');
                    else
                      _holidays[element] = ['Day $i'];
                  }
                });
              });
              print(_holidays);
              setState(() {
                _progressController = false;
              });
            }
          });
        });
      });
    });
  }

  updateDb() async {
    _firestore
        .collection("users")
        .where('username', isEqualTo: _currentUserUsername)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        _firestore
            .collection("users")
            .document(result.documentID)
            .collection("events1")
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
//              print(result.data);
            eventsDb.forEach((key, value) {
              result.reference.updateData({key: FieldValue.arrayUnion(value)});
            });

//            print('finished');
          });
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.purple[50], Colors.purple[200]],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
      ),
      child: Scaffold(
          backgroundColor: Colors.deepPurple[200],
          body: SafeArea(
            child: _progressController
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(
                          backgroundColor: Colors.deepPurple[200],
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              Colors.deepPurple[200]),
                          strokeWidth: 5,
                        ),
                        Text(
                          'Breathe in, Breathe out!',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 21.0,
                            letterSpacing: 2.0,
                            fontFamily: 'PermanentMarker',
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  )
                : Column(
                    children: <Widget>[
                      TableCalendar(
                        formatAnimation: FormatAnimation.slide,
                        availableCalendarFormats: {CalendarFormat.month: ''},
                        initialCalendarFormat: CalendarFormat.month,
                        events: _events,
                        holidays: _holidays,
                        daysOfWeekStyle: DaysOfWeekStyle(
                            weekdayStyle: TextStyle(
                              fontFamily: 'Quicksand',
                              color: Colors.white,
                              fontSize: 10.0,
                              fontWeight: FontWeight.w600,
                            ),
                            weekendStyle: TextStyle(
                                fontFamily: 'Quicksand',
                                color: Colors.white,
                                fontSize: 10.0,
                                fontWeight: FontWeight.w600)),
                        calendarStyle: CalendarStyle(
                          markersColor: Colors.white,
                          selectedColor: Colors.white,
                          todayColor: Color(0xff8675a9),
                          todayStyle: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                          ),
                          weekendStyle: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          weekdayStyle: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          outsideDaysVisible: false,
                          renderDaysOfWeek: true,
                          holidayStyle: TextStyle(
                              color: Colors.pink[100],
                              fontFamily: 'Quicksand',
                              fontWeight: FontWeight.w600),
                          selectedStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            color: Color(0xff8675a9),
                            fontWeight: FontWeight.w800,
                            fontSize: 13.0,
                          ),
                        ),
                        headerStyle: HeaderStyle(
                          leftChevronIcon: Icon(
                            Icons.chevron_left,
                            color: Colors.white,
                          ),
                          rightChevronIcon: Icon(
                            Icons.chevron_right,
                            color: Colors.white,
                          ),
                          headerMargin: EdgeInsets.fromLTRB(20, 20, 20, 50),
                          titleTextStyle: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 25.0,
                              color: Colors.white),
                          centerHeaderTitle: true,
                          formatButtonDecoration: BoxDecoration(
                            color: Colors.blue[100],
                            borderRadius: BorderRadius.circular(40.0),
                          ),
                          formatButtonTextStyle: TextStyle(
                            fontFamily: 'Lato',
                            fontSize: 10.0,
                          ).copyWith(color: Colors.white, fontSize: 13.0),
                          formatButtonShowsNext: true,
                        ),
                        startingDayOfWeek: StartingDayOfWeek.sunday,
                        onDaySelected: (day, events) {
                          setState(() {
                            _selectedEvents = events;
                            datesStream();
                          });
                        },
                        calendarController: _controller,
                      ),
                      Expanded(child: _buildEventList()),
                    ],
                  ),
          ),
          floatingActionButton: FloatingActionButton(
              backgroundColor: Colors.pink[200],
              child: Icon(Icons.add),
              onPressed: _showAddDialog),
          bottomNavigationBar: BottomNavBar()),
    );
  }

  _showAddDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        title: Text('Add Note'),
        titleTextStyle: TextStyle(
            fontSize: 20.0, fontFamily: 'Quicksand', color: Colors.black),
        content: TextField(
          textCapitalization: TextCapitalization.sentences,
          controller: _eventController,
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              if (_eventController.text.isEmpty) return;
              setState(() {
                if (_events[_controller.selectedDay] != null) {
                  _events[_controller.selectedDay].add(_eventController.text);
                } else {
                  _events[_controller.selectedDay] = [_eventController.text];
                }
                String name =
                    '${_controller.selectedDay.day}-${_controller.selectedDay.month}-${_controller.selectedDay.year}';
                if (eventsDb[name] != null) {
                  eventsDb[name].add(_eventController.text);
                } else {
                  eventsDb[name] = [_eventController.text];
                }
                print(eventsDb);
                updateDb();
                _eventController.clear();
                Navigator.pop(context);
                StrutStyle(
                  fontFamily: 'Quicksand',
                );
              });
            },
            child: Column(
              children: <Widget>[
                Text(
                  'Save',
                  style: TextStyle(
                      fontFamily: 'Quicksand',
                      fontWeight: FontWeight.bold,
                      color: Colors.pink[200]),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Color(0xff59405c),
                      offset: const Offset(3.0, 3.0),
                      blurRadius: 5.0,
                      spreadRadius: 2.0,
                    ),
                  ],
                  color: Colors.white,
                  border: Border.all(width: 0.8),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                margin:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: ListTile(
                  title: Text(event.toString(),
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'Lato',
                          fontSize: 15.0)),
                ),
              ))
          .toList(),
    );
  }
}
