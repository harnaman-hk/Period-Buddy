import 'package:flowSure/ui/user/user.dart';
import 'package:flutter/material.dart';
import 'package:flowSure/ui/layouts/navbar.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'estimatedperiod.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';

class Calendar extends StatefulWidget {
  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  CalendarController _controller;
  final _auth = FirebaseAuth.instance;
  String _currentUserUsername = "";
  bool _progressController = true;

  final _firestore = Firestore.instance;
  final controller = TextEditingController();

  Map<DateTime, List<dynamic>> _events = {};
  Map<DateTime, List<dynamic>> _holidays = {};
  Map<String, List> periodEvents = {};
  Map<String, List> predictEvents = {};
  Map<String, String> eventText = {};
  List eve = [];

  datesStream() async {
//    print('enterd fun');
    _firestore
        .collection("users")
        .where('username', isEqualTo: _currentUserUsername)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
//        print(result.data);
        _firestore
            .collection("users")
            .document(result.documentID)
            .collection("events")
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
//            print(result.data['Period']);
            if (result.data['Period'] != null) {
              setState(() {
                _progressController = false;
              });
              Map<dynamic, dynamic> li = {};
              Map<String, List> db = {};
              li = (result.data['Period']);
//              print("list$li");
              li.forEach((key, value) {
                db[key] = value;
              });
              db.forEach((key, value) {
                List list = value;
                periodEvents[key] = [];
                DateTime maxDate;
                list.forEach((element) {
                  Timestamp time = element;
                  DateTime p = DateTime.fromMicrosecondsSinceEpoch(
                      time.microsecondsSinceEpoch);
                  periodEvents[key].add(p);
                });
              });
              predictioninit();
//              print('period $periodEvents, predict $predictEvents');
              Estimateperiod est = Estimateperiod();
              eve = est.createEvents(periodEvents, predictEvents);
              for (int i = 0; i < eve.length; i++) {
                DateTime time = eve[i];
                DateTime element = eve[i];
                if (_events[time] != null)
                  _events[time].add('Day 1');
                else
                  _events[time] = ['Day 1'];

                for (var i = 2; i <= 5; i++) {
                  time = time.add(Duration(days: 1));
                  if (_events[time] != null)
                    _events[time].add('Day $i');
                  else
                    _events[time] = ['Day $i'];
                }

                time = element.add(Duration(days: 14));
                if (_holidays[time] != null)
                  _holidays[time].add('Day of\nOvulation');
                else
                  _holidays[time] = ['Day of\nOvulation'];
              }
              textup();
            }
          });
        });
      });
    });
  }

  predictioninit() {
    Estimateperiod obj = Estimateperiod();
    predictEvents = obj.createArray1(periodEvents, monthsInYear);
  }

  updateDb() {
    _firestore
        .collection("users")
        .where('username', isEqualTo: _currentUserUsername)
        .getDocuments()
        .then((querySnapshot) {
      querySnapshot.documents.forEach((result) {
        _firestore
            .collection("users")
            .document(result.documentID)
            .collection("events")
            .orderBy('Period')
            .getDocuments()
            .then((querySnapshot) {
          querySnapshot.documents.forEach((result) {
            if (result.data.containsKey('Period')) {
//              print(result.data);
              periodEvents.forEach((key, value) {
                result.reference
                    .updateData({'Period.$key': FieldValue.arrayUnion(value)});
              });
            }
//            print('finished');
          });
          _events = {};
          _holidays = {};
          predictEvents = {};
          periodEvents = {};
          eve = [];

          datesStream();

          eventText = {};
          datesStream();
        });
      });
    });
  }

  textup() {
    List eventt = [];
    _events.forEach((key, value) {
      String name = '${key.day}-${key.month}-${key.year}';
      eventText[name] = value.first;
    });
    _holidays.forEach((key, value) {
      String name = '${key.day}-${key.month}-${key.year}';
      eventText[name] = value.first;
    });
    periodEvents.forEach((key, value) {
      List li = value;
      li.forEach((element) {
        DateTime d = element;
        eventt.add(d);
      });
    });
    predictEvents.forEach((key, value) {
      List li = value;
      li.forEach((element) {
        DateTime d = element;
        eventt.add(d);
      });
    });
    eventt.sort((a, b) => a.compareTo(b));
//    print(eventt);
    for (int i = 0; i < eventt.length; i++) {
      DateTime time = eventt[i];
      int n = 28;
      DateTime ovuDate = eventt[i].add(Duration(days: 14));
      DateTime endingdate = eventt[i].add(Duration(days: 28));
      if (i < eventt.length - 1) {
        n = time.difference(eventt[i + 1]).inDays.abs();
        endingdate = eventt[i + 1];
      }
//      print('n $n end $endingdate ov $ovuDate time $time\n');
      for (int i = 1; i <= n; i++) {
        time = time.add(Duration(days: 1));
        String name = '${time.day}-${time.month}-${time.year}';
        if (eventText[name] == null) {
          if (time.isBefore(ovuDate)) {
            if ((time.difference(ovuDate).inHours.abs() / 24).round() == 1)
              eventText[name] = 'Ovulation in\n1 day';
            else
              eventText[name] =
                  'Ovulation in\n${(time.difference(ovuDate).inHours.abs() / 24).round()} days';
          } else {
//            print(
//                '$time $endingdate ${(time.difference(endingdate).inHours.abs() / 24).round()}');
            if ((time.difference(endingdate).inHours.abs() / 24).round() <= 1)
              eventText[name] = 'Period in\n1 day';
            else
              eventText[name] =
                  'Period in\n${(time.difference(endingdate).inHours.abs() / 24).round()} days';
          }
        }
//        print('$name ${eventText[name]}\n');
      }
    }
  }

  @override
  void initState() {
    super.initState();
    _controller = CalendarController();
    initUser();
  }

  initUser() async {
    FirebaseUser _user = await _auth.currentUser();
    final firestoreInstance = Firestore.instance;
    firestoreInstance
        .collection("users")
        .document(_user.uid)
        .get()
        .then((value) {
      print(value.data);
      _currentUserUsername = value["username"];
      datesStream();
    });
  }

  DateTime selectedDate = DateTime.now();
  DateTime period = DateTime.now();

  Map<String, dynamic> encodeMap(Map<DateTime, dynamic> map) {
    Map<String, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[key.toString()] = map[key];
    });
    return newMap;
  }

  Map<DateTime, dynamic> decodeMap(Map<String, dynamic> map) {
    Map<DateTime, dynamic> newMap = {};
    map.forEach((key, value) {
      newMap[DateTime.parse(key)] = map[key];
    });
    return newMap;
  }

  Map<int, String> monthsInYear = {
    1: "January",
    2: "February",
    3: "March",
    4: 'April',
    5: 'May',
    6: 'June',
    7: "July",
    8: "August",
    9: "September",
    10: "October",
    11: "November",
    12: "December"
  };

  String textInPinkCircle() {
    if (eventText.length == 0) {
      textup();
    }
    String name =
        '${selectedDate.day}-${selectedDate.month}-${selectedDate.year}';
    if (eventText[name] != null) {
      return eventText[name];
    }
    return 'Log in your period!';
  }

  Color changecolor() {
    String name =
        '${selectedDate.day}-${selectedDate.month}-${selectedDate.year}';
    if (eventText[name] != null) {
      if (eventText[name] == 'Day of\nOvulation')
        return Color(0xffa6dcef);
      else if (eventText[name].substring(0, 3) == 'Day')
        return Color(0xffff7171);
    }
    return Color(0xffffd5cd);
  }

  bool periodshow() {
    String name =
        '${selectedDate.day}-${selectedDate.month}-${selectedDate.year}';
    if (eventText[name] != null) {
      if (eventText[name] != 'Day of\nOvulation' &&
          eventText[name].substring(0, 3) == 'Day') return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/pic.jpg'), fit: BoxFit.cover),
        // gradient: LinearGradient(
        //     begin: Alignment.topRight,
        //     end: Alignment.bottomLeft,
        //     colors: [Colors.pink[100], Color(0xff8675a9)]),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: _progressController
            ? Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(
                      backgroundColor: Colors.pink[50],
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.pink[50]),
                      strokeWidth: 5,
                    ),
                    Text(
                      "How you doin'?!",
                      style: TextStyle(
                        letterSpacing: 2.0,
                        color: Colors.black,
                        fontSize: 24.0,
                        fontFamily: 'PermanentMarker',
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              )
            : ListView(children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    TableCalendar(
                      onCalendarCreated: (DateTime first, DateTime last,
                          CalendarFormat format) {
                        print(first);
                        print('enteretd');
                        print(_events);
                      },
                      builders: CalendarBuilders(
                        markersBuilder: (context, date, events, holidays) {
                          final children = <Widget>[];

                          if (events.isNotEmpty) {
                            children.add(
                              Positioned(
//                          right: 1,
                                bottom: 0,
                                child: Icon(
                                  Icons.opacity,
                                  color: Color(0xffe7305b),
                                  size: 18.0,
                                ),
                              ),
                            );
                          }

                          if (holidays.isNotEmpty) {
                            children.add(
                              Positioned(
//                          right: -2,
                                bottom: -5,
                                child: Icon(
                                  Icons.lens,
                                  color: Colors.blueAccent,
                                  size: 10.0,
                                ),
                              ),
                            );
                          }

                          return children;
                        },
                      ),
                      onDaySelected: (day, events) {
                        setState(() {
                          selectedDate = day;
                        });
                      },
                      holidays: _holidays,
                      events: _events,
                      initialCalendarFormat: CalendarFormat.week,
                      formatAnimation: FormatAnimation.slide,
                      availableCalendarFormats: {CalendarFormat.week: ''},
                      daysOfWeekStyle: DaysOfWeekStyle(
                          weekdayStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontSize: 10.0,
                            fontWeight: FontWeight.normal,
                          ),
                          weekendStyle: TextStyle(
                              fontFamily: 'Quicksand',
                              fontSize: 10.0,
                              fontWeight: FontWeight.bold)),
                      calendarStyle: CalendarStyle(
//                    markersMaxAmount: 2,
                          holidayStyle: TextStyle(
                            color: Colors.blueAccent,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          outsideHolidayStyle: TextStyle(
                            color: Colors.redAccent,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          outsideWeekendStyle: TextStyle(
                            color: Colors.black,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          weekendStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          unavailableStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          weekdayStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          outsideStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 12.0,
                          ),
                          outsideDaysVisible: false,
                          markersColor: Colors.redAccent,
                          canEventMarkersOverflow: true,
                          selectedColor: Colors.white,
                          todayColor: Colors.red[50],
                          selectedStyle: TextStyle(
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.normal,
                            fontSize: 13.0,
                          ),
                          todayStyle: TextStyle(
                            backgroundColor: Colors.red[50],
                          )),
                      headerStyle: HeaderStyle(
                        headerMargin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                        titleTextStyle: TextStyle(
                          fontSize: 20.0,
                          fontFamily: 'Quicksand',
                        ),
                        centerHeaderTitle: true,
                        formatButtonDecoration: BoxDecoration(
                          color: Colors.blue[100],
                          borderRadius: BorderRadius.circular(40.0),
                        ),
                        formatButtonTextStyle: TextStyle(
                          fontFamily: 'Quicksand',
                          fontSize: 10.0,
                        ).copyWith(color: Colors.black, fontSize: 13.0),
                        formatButtonShowsNext: true,
                      ),
                      calendarController: _controller,
                    ),
                    SizedBox(height: 80.0),
                    Center(
                      child: Container(
                        child: Column(
                          children: <Widget>[
                            SizedBox(height: 25.0),
                            Text(
                              DateFormat('EEEE').format(selectedDate) +
                                  "," +
                                  monthsInYear[selectedDate.month] +
                                  " " +
                                  selectedDate.day.toString(),
                              softWrap: true,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13.0,
                                fontFamily: 'Quicksand',
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                            SizedBox(
                              height: 30.0,
                              width: 150,
                              child: Divider(
                                thickness: 1,
                                color: Colors.white60,
                              ),
                            ),
                            periodshow()
                                ? Text(
                                    'Period:',
                                    style: TextStyle(
                                        fontFamily: 'Gayathri',
                                        fontSize: 25.0,
                                        color: Colors.black),
                                  )
                                : Container(),
                            SizedBox(
                              height: 18.0,
                            ),
                            Expanded(
                              child: Text(
                                textInPinkCircle(),
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: 'Gayathri',
                                    fontStyle: FontStyle.normal,
                                    fontSize: 33.0,
                                    color: Colors.black),
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            Container(
                              width: 110.0,
                              height: 30.0,
                              margin: EdgeInsets.only(bottom: 0.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(40.0),
                                  bottomRight: Radius.circular(40.0),
                                  topLeft: Radius.circular(40.0),
                                  bottomLeft: Radius.circular(40.0),
                                ),
                              ),
                              child: FlatButton(
                                splashColor: Colors.red[100],
                                onPressed: () {
                                  period = selectedDate;
                                  String fieldname =
                                      (monthsInYear[period.month])
                                          .substring(0, 3)
                                          .toLowerCase();
                                  fieldname = ('$fieldname-${period.year}');
                                  if (periodEvents[fieldname] == null)
                                    periodEvents[fieldname] = [period];
                                  else {
                                    bool present = false;
                                    List l = periodEvents[fieldname];
                                    l.forEach((element) {
                                      if (period.difference(element).inDays >
                                          1) {
                                      } else {
                                        present = true;
                                      }
                                    });
                                    if (!present)
                                      periodEvents[fieldname].add(period);
                                  }

                                  updateDb();
                                  Navigator.pushReplacementNamed(
                                      context, '/calendar');
                                },
                                child: Text(
                                  "Log Period",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'Quicksand',
                                      color: Colors.red,
                                      fontSize: 13.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ],
                        ),

                        //margin: EdgeInsets.all(50.0),
                        width: 300.0,
                        height: 300.0,

                        decoration: BoxDecoration(
                          color: changecolor(),
                          shape: BoxShape.circle,
                          border: new Border.all(
                            color: Colors.white,
                            width: 7,
                          ),
                        ),
                        padding: EdgeInsets.all(25.0),
                        margin: EdgeInsets.all(7.0),
                      ),
                    ),
                  ],
                ),
              ]),
        floatingActionButton: FloatingActionButton(
            child: Icon(
              Icons.calendar_today,
            ),
            backgroundColor: Color(0xff8675a9),
            onPressed: () {
              datesStream();
//              print('periods:$periodEvents,predict:$predictEvents');
              Navigator.pushReplacementNamed(context, '/calendar_month');
            }),
        bottomNavigationBar: BottomNavBar(),
      ),
    );
  }
}
