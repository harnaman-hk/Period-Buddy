import 'package:flowSure/ui/exercise_section/constants.dart';
import 'package:flowSure/ui/layouts/constants.dart';
import 'package:flowSure/ui/layouts/loading.dart';
import 'package:flowSure/ui/user/auth.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  final Function toggleView;
  SignIn({this.toggleView});
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final AuthService _auth = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool loading = false;

  String username = "";
  String password = "";
  String error = "";

  @override
  Widget build(BuildContext context) {
    final initialHeight = MediaQuery.of(context).size.height;
    print(initialHeight);
    return loading
        ? Loading()
        : Scaffold(
            // backgroundColor: Color(0xFFfdf0f2),
            // backgroundColor: Colors.red[50],
            backgroundColor: Colors.red[50],
            resizeToAvoidBottomInset: false,
            body: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    child: Column(
                      children: <Widget>[
                        SafeArea(
                          child: Container(
                            padding: EdgeInsets.only(
                                top: 8.0, left: 50.0, right: 50.0),
                            height: MediaQuery.of(context).size.height * 0.56,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "FlowSure",
                                  style: TextStyle(
                                    fontSize: 55.0,
                                    color: Colors.pink[300],
                                    fontFamily: "Lobster",
                                  ),
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                Form(
                                  key: _formKey,
                                  child: Column(
                                    children: <Widget>[
                                      TextFormField(
                                        validator: (val) => val.isEmpty
                                            ? ''
                                            : null,
                                        onChanged: (val) {
                                          setState(() {
                                            username = val;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.grey),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.pink[200])),
                                          border: OutlineInputBorder(),
                                          labelText: "Enter Username",
                                          labelStyle: TextStyle(
                                              color: Colors.pink[200],
                                              fontSize: 18.0),
                                        ),
                                      ),
                                      TextFormField(
                                        validator: (val) => val.isEmpty
                                            ? ''
                                            : null,
                                        onChanged: (val) {
                                          setState(() {
                                            password = val;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          enabledBorder: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.grey),
                                          ),
                                          focusedBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: Colors.pink[100])),
                                          border: OutlineInputBorder(),
                                          labelText: "Enter Password",
                                          labelStyle: TextStyle(
                                              color: Colors.pink[200],
                                              fontSize: 18.0),
                                        ),
                                        obscureText: true,
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 20.0,),
                                FlatButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(20.0),
                                    side:
                                        BorderSide(color: Colors.pink[300]),
                                  ),
                                  color: Colors.red[50],
                                  textColor: Colors.pink[300],
                                  padding: EdgeInsets.all(8.0),
                                  child: Text(
                                    "Sign In".toUpperCase(),
                                    style: TextStyle(fontSize: 18.0),
                                  ),
                                  onPressed: () async {
                                    if (_formKey.currentState.validate()) {
                                      setState(() {
                                        loading = true;
                                      });
                                      dynamic result =
                                          await _auth.signInWithUsernameAndPassword(username, password);
                                      if (result == null) {
                                        setState(() {
                                          error =
                                              "Password should be atleast 6 characters long";
                                          loading = false;
                                        });
                                      }
                                    }
                                  },
                                ),
                                SizedBox(
                                  height: 18.0,
                                ),
                                GestureDetector(
                                  child: Text(
                                    "Register",
                                    style: TextStyle(
                                      color: Colors.pink[200],
                                      fontSize: 15.0,
                                    ),
                                  ),
                                  onTap: () => widget.toggleView(),
                                ),
                                SizedBox(
                                  height: 12.0,
                                ),
                                Text(
                                  error,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 14.0),
                                ),
                              ],
                            ),
                          ),
                        ),
                        if (MediaQuery.of(context).size.height > 500.0)
                        Container(
                          // margin: EdgeInsets.only(top: 24.0),
                          height: MediaQuery.of(context).size.height * 0.40,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(50.0),
                                topRight: Radius.circular(50.0)),
                            color: Colors.pink[200],
                            image: DecorationImage(
                              image: AssetImage("assets/images/period.gif"),
                              alignment: Alignment.bottomRight,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
  }
}
