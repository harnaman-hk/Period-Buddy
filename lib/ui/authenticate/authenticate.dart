import 'package:flowSure/ui/authenticate/register.dart';
import 'package:flowSure/ui/authenticate/sign_in.dart';
import 'package:flowSure/ui/calendar/calendar_home.dart';
import 'package:flutter/material.dart';

class Authenticate extends StatefulWidget {
  @override
  _AuthenticateState createState() => _AuthenticateState();
}
  
class _AuthenticateState extends State<Authenticate> {

  bool showSignIn = false;
  void toggleView() {
    setState(() {
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    if (showSignIn) {
      return SignIn(toggleView: toggleView);
    } else {
      return RegisterScreen(toggleView: toggleView,);
    }
  }
}